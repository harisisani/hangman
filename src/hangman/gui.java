package hangman;

import java.awt.event.*;
import javax.swing.*;
import java.awt.*;

	public  class gui extends keyboard implements ActionListener
	{
		
		
		
		public JFrame hgui()
		
		
		{
			
			
			
			// Creating Fonts
			
			Font font = new Font("georgia",Font.BOLD,30);
			Font font3 = new Font("Calibri (Body)",Font.ITALIC,15);
			Font font4 = new Font("Calibri (Body)",Font.PLAIN,30);
			Font font5 = new Font("Calibri (Body)",Font.BOLD,20);
			Font font6 = new Font("georgia",Font.BOLD + Font.ITALIC,30);
			Font font7 = new Font("Calibri (Body)",Font.BOLD,30);
			Font font8 = new Font("Calibri (Body)",Font.BOLD,90);
		
						
			 // Creating Frame //
			
			
			frame = new JFrame ("THE HANGMAN GAME");		
			frame.setSize (1300,700);
		    frame.setLocation(70,25);
		    ImageIcon ficon = new ImageIcon("icon.jpg");
		    frame.setIconImage(ficon.getImage());
		  // Creating Panels //
		    ImageIcon name = new ImageIcon("panel2.jpg");
				panel2 = new JLabel(name);
				start_game_panel = new JPanel();
				start_game_panel.setLayout(null);
				pic = new JPanel();
				pic.setBounds(950, -10, 315, 400);
				 
				
	
	    
		// Creating start game button
				
				
				
				start = new JButton("START GAME");
				start.setBounds(725,350,250,210);
				start.setForeground(Color.RED);
				start.setFont(font8);
				
			start.setFocusPainted(false);
		       start.setBorderPainted(false);
		       start.setContentAreaFilled(false);
		        ImageIcon startimage = new ImageIcon("start.jpg");
		      
		        start_game_label = new JLabel(startimage);
		        start_game_label.setBounds(0, 0, 1300, 700);
				start_game_label.add(start);
				start_game_panel.add(start_game_label);
				frame.add(start_game_panel);
			
				
				
	    
		 // Creating Menu Bar // 
		
		    JMenuBar menubar = new JMenuBar();
		    
		// Creating Menus //
		
		    menu1 = new JMenu("Game");
		    menu3= new JMenu("Help");
		    
		    
		
		// Creating Menu Items of Menu "Game" //
		
		    JMenuItem menuitem1 = new JMenuItem("Exit To Windows");
		
		// Handling of menu item exit
		    
		   
			menuitem1.addActionListener(new ActionListener()
			{public void actionPerformed(ActionEvent e)
			
			{
					System.exit(0);
				
			}
			});
			
		//handling of start button and new game menu item
			
			start.addActionListener(new ActionListener()
			{public void actionPerformed(ActionEvent e)
				
			{
				 
				Toolkit.getDefaultToolkit().beep();
				start.setText("");
				frame.remove(start_game_panel);
				menuitem1.addActionListener((ActionListener) this);
				frame.add(player_name_panel);
				
			}
			});
		
		// Creating Menu Item of Menu "Help" //
		
		   JMenuItem menuitem3 = new JMenuItem("Instructions");
		   
		// Handling of instructions
		   
		    menuitem3.addActionListener(new ActionListener()
			{public void actionPerformed(ActionEvent e)
			
			{
				ins();
				
			}
			});
		    
		 // Inserting menus in menu bar //
			
		 			menubar.add(menu1);
		 			menubar.add(menu3);
			
		// Inserting menu items in menus // 
			
			menu1.add(menuitem1);
			menu3.add(menuitem3);
			
	   // Placing Menu bar in Frame
			
			frame.setJMenuBar(menubar);
			
			
							
			//creating label for telling step
				step = new JLabel("player one enter the word for player two ");
				step.setBounds(200,10, 1000, 50);
				step.setFont(font);
			
		
				
			// creating textfield for showing the entered character
				character = new JTextField();
				character.setBounds(620,100,52, 50);
				character.setFont(font7);
				character.setBackground(Color.yellow);
				character.setEditable(false);
				
			
				
			// creating text feild for hint
				
			hint = new JTextField();
			hint.setBackground(Color.LIGHT_GRAY);
			hint.setBounds(70, 100 , 500, 50);
			hint.setFont(font6);
			hint.setText("HINT: ");
					
				
			// creating textfield for showing the instructions
				
			instructions = new JTextField();
			instructions.setBounds(220, 285, 580, 45);
			instructions.setBackground(Color.LIGHT_GRAY);
			instructions.setFont(font);
			instructions.setEditable(false);
			instructions.setBorder(null);
			
			
			
			// creating textfield for word guessing and word entering
			 dialog = new JTextField();
			 dialog.setBackground(Color.LIGHT_GRAY);
			 dialog.setBounds(70,170,880,50);
			 dialog.setFont(font);
			 dialog1 = new JPasswordField();
			 dialog1.setBackground(Color.LIGHT_GRAY);
			 dialog.setEditable(false);
			 dialog1.setBounds(70,170,880,50);
			 dialog1.setFont(font);
			 auto = new JButton("AUTO GENERATE");
			 auto.setBounds(720,100,200, 50);
			 auto.addActionListener((ActionListener)this);
			 panel2.add(auto);
		
			 
			// Buttons for entering words
			 
			 done = new JButton(" ENTER ");
			 done.setFont(font5);
			 done.setBounds(450,230, 130, 50);
			 done.addActionListener((ActionListener) this);
			 done.setEnabled(false);
			 // Disabling Enter button in main game window
			
			 dialog1.addKeyListener(new KeyAdapter() 
					 {
					@SuppressWarnings("deprecation")
					public void keyReleased(KeyEvent e) 
			        
			        { //watch for key strokes
			            if((dialog1.getText().length() <=2))
			            {
			                done.setEnabled(false);
			            }
			            else
			            
			                done.setEnabled(true);
			            }
			 });
			
			 // for name of player 1
			 player1 = new JTextField();
			 player1.setText("Player 1");
			 player1.setFont(font3);
			 player1.setForeground(Color.gray);
			 player1.setBounds(850, 270, 300, 50);
			   
		
				
			
			
			 player1.addMouseListener(new MouseAdapter()
				{public void mouseClicked(MouseEvent e)
				
				{	
						P1.setEnabled(false);
						player1.setText("");
						player1.setFont(font4);
						player1.setForeground(Color.BLACK);

					
				}
				});
			 
			 
			 // for name of player 2
			 player2 = new JTextField();
			 player2.setText("Player 2");
			 player2.setFont(font3);
			 player2.setForeground(Color.gray);
			 player2.setBounds(850, 450, 300, 50);
			 
			 player2.addMouseListener(new MouseAdapter()
				{public void mouseClicked(MouseEvent e)
				
				{
					P1.setEnabled(false);
						player2.setText("");
						player2.setFont(font4);
						player2.setForeground(Color.BLACK);
				}
				}); 
			 
			 			 
			 
			  
			 score= new JLabel();
			 score.setBounds(145, 580, 400, 40);
			 score.setBorder(null);
			 score.setFont(font5);
			 JLabel jscore = new JLabel("Scores:");
			 jscore.setBounds(20, 575, 200, 50);
			 jscore.setFont(font);
			 
			// buttons for players name finalizing
			
			 P1 = new JButton("ENTER");
			 P1.setFont(font);
			 P1.setBounds(700,540, 200, 50);
			 P1.addActionListener((ActionListener) this);
			 
			 // Disabling Enter Button in Player name pannel 
			 
			 player1.addKeyListener(new KeyAdapter() 
			 {
			        public void keyReleased(KeyEvent e) 
			        
			        { //watch for key strokes
			            if(player1.getText().length() == 0 || player2.getText().length() == 0 )
			            {
			            	P1.setEnabled(false);
			            	}
			            else
			            {
			                P1.setEnabled(true);
			            }
			        }
			 });
			 
			 player2.addKeyListener(new KeyAdapter() 
			 {
			        public void keyReleased(KeyEvent e) 
			        
			        { //watch for key strokes
			            if(player2.getText().length() == 0 || player1.getText().length() == 0  )
			                P1.setEnabled(false);
			            else if (player1.getText().length() != 0 && player2.getText().length() != 0)
			            {
			                P1.setEnabled(true);
			            }
			        }
			 });


			 player1.setBackground(Color.LIGHT_GRAY);
			 player2.setBackground(Color.LIGHT_GRAY);
			 
			 // labels of players
			 
			 
			 
			 //crating textfield for copying the word of player one to a variable named a
		
				// Creating Buttons and setting their bounds//
			 	 arrow = new JButton("PROCEED");
				 arrow.setBounds(375, 525, 360, 50);
				 arrow.setFont(font5);
				 start.setFont(font);

			
				 
				 
		        Q = new JButton("Q");
		        Q.setBounds(285, 370 ,50,45);
		        Q.addActionListener((ActionListener) this);
		      
		      	W = new JButton("W");
		        W.setBounds(340, 370 ,50,45);
		        W.addActionListener((ActionListener) this);
		        
			    E = new JButton("E");
			    E.setBounds(395, 370 ,50,45);
			    E.addActionListener((ActionListener) this); 
			    
			    R = new JButton("R");
			    R.setBounds(450, 370 ,50,45);
			    R.addActionListener((ActionListener) this);
			    
			    T = new JButton("T");
			    T.setBounds(505, 370 ,50,45);
			    T.addActionListener((ActionListener) this);
			   
			    Y = new JButton("Y");
			    Y.setBounds(560, 370 ,50,45);
			    Y.addActionListener((ActionListener) this);
			    
			    U = new JButton("U");
			    U.setBounds(615, 370 ,50,45);
			    U.addActionListener((ActionListener) this);
			    
			    I = new JButton("I");
			    I.setBounds(670, 370 ,50,45);
			    I.addActionListener((ActionListener) this);
			    
			    O = new JButton("O");
			    O.setBounds(725, 370 ,50,45);
			    O.addActionListener((ActionListener) this);
			    
			    P = new JButton("P");
			    P.setBounds(780, 370 ,50,45);
			    P.addActionListener((ActionListener) this);
			    
			    A = new JButton("A");
			    A.setBounds(310, 420 ,50,45);
			    A.addActionListener((ActionListener) this);
			    
			    S = new JButton("S");
			    S.setBounds(365, 420 ,50,45);
			    S.addActionListener((ActionListener) this);
			    
			    D = new JButton("D");
			    D.setBounds(420, 420 ,50,45);
			    D.addActionListener((ActionListener) this);
			    
			    F = new JButton("F");
			    F.setBounds(475, 420 ,50,45);
			    F.addActionListener((ActionListener) this);
			    
			    G = new JButton("G");
			    G.setBounds(530, 420 ,50,45);
			    G.addActionListener((ActionListener) this);
			    
			    H = new JButton("H");
			    H.setBounds(585, 420 ,50,45);
			    H.addActionListener((ActionListener) this);
			    
			    J = new JButton("J");
			    J.setBounds(640, 420 ,50,45);
			    J.addActionListener((ActionListener) this);
			    
			    K = new JButton("K");
			    K.setBounds(695, 420 ,50,45);
			    K.addActionListener((ActionListener) this);
			    
			    L = new JButton("L");
			    L.setBounds(750, 420 ,50,45);
			    L.addActionListener((ActionListener) this);
			    
			    Z = new JButton("Z");
			    Z.setBounds(365, 470 ,50,45);
			    Z.addActionListener((ActionListener) this);
			    
			    X = new JButton("X");
			    X.setBounds(420, 470 ,50,45);
			    X.addActionListener((ActionListener) this);
			    
			    C = new JButton("C");
			    C.setBounds(475, 470 ,50,45);
			    C.addActionListener((ActionListener) this);
			    
			    V = new JButton("V");
			    V.setBounds(530, 470 ,50,45);
			    V.addActionListener((ActionListener) this);
			    
			    B = new JButton("B");
			    B.setBounds(585, 470 ,50,45);
			    B.addActionListener((ActionListener) this);
			    
			    N = new JButton("N");
			    N.setBounds(640, 470 ,50,45);
			    N.addActionListener((ActionListener) this);
			    
			    M = new JButton("M");
			    M.setBounds(695, 470 ,50,45);
			    M.addActionListener((ActionListener) this);
			    name();
			   
		  // Inserting Buttons in Panel 2 //
			panel2.add(arrow);
			panel2.add(score);
			panel2.add(jscore);
			panel2.add(Q);
			panel2.add(W);
			panel2.add(E);
			panel2.add(R);
			panel2.add(T);
			panel2.add(Y);
			panel2.add(U);
			panel2.add(I);
			panel2.add(O);
			panel2.add(P);
			panel2.add(A);
			panel2.add(S);
			panel2.add(D);
			panel2.add(F);
			panel2.add(G);
			panel2.add(H);
			panel2.add(J);
			panel2.add(K);
			panel2.add(L);
			panel2.add(Z);
			panel2.add(X);
			panel2.add(C);
			panel2.add(V);
			panel2.add(B);
			panel2.add(N);
			panel2.add(M);
			panel2.add(character);
			panel2.add(instructions);
			panel2.add(dialog1);
			panel2.add(done);
			panel2.add(step);
			panel2.add(hint);
			
			// Player_name_panel setting
			 
			 
			player_name_panel = new JPanel();
			ImageIcon panel = new ImageIcon("name.jpg");
			 name_label = new JLabel(panel);
			 player_name_panel.add(name_label);
			 name_label.setBounds(0, 0, 1300, 700);
			name_label.add(player1);
			name_label.add(player2);
			name_label.add(P1);
			
			
			
			
			
			
			
			
			
			
			
			
			
			//panel2.add(P2);
			// Adding panel2 in frame //
			start_game_panel.setLayout(null);
			player_name_panel.setLayout(null);
			//frame.add(panel_two);
		panel2.setLayout(null);
			
			
			
			//  //
			return frame;
			
		}

	
		}


